#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termbox.h>

int main( int argc, char *argv[]  )
{

  FILE *fp;
  char path[1035];
  int j;

  (void)argc; (void)argv;
  int code = tb_init();
  if (code < 0) {
    fprintf(stderr, "termbox init failed, code: %d\n", code);
    return -1;
  }

  tb_select_input_mode(TB_INPUT_ESC);
  tb_select_output_mode(TB_OUTPUT_NORMAL);
  tb_clear();

  /* Open the command for reading. */
  fp = popen("/bin/ls .", "r");
  if (fp == NULL) {
    printf("Failed to run command\n" );
    exit(1);
  }

  /* Read the output a line at a time - output it. */
  while (fgets(path, sizeof(path)-1, fp) != NULL) {
    while (path[j] != 0) {
      tb_change_cell(j, 0, path[j], TB_WHITE, TB_BLACK);
      j++;
    }
  }

  /* close */
  pclose(fp);
  
  tb_present();

  while (1) {
    struct tb_event ev;
    tb_poll_event(&ev);

    if (ev.key == TB_KEY_ESC) {
      tb_shutdown();
      return 0;
    }
  }
}
